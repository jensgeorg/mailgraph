#!/usr/bin/env python3

import argparse
import logging
import os
import os.path
import re
import rrdtool
from systemd import journal


class Mailgraph:
    def __init__(self):
        self.rrd = 'mailgraph.rrd'
        self.rrd_virus = 'mailgraph_virus.rrd'
        self.rrd_greylist = 'mailgraph_greylist.rrd'
        self.rrd_inited = False
        self.sum = dict(sent=0, received=0, bounced=0, rejected=0, virus=0, spam=0, greylisted=0, delayed=0)
        self.rrd_initialized = False
        self.this_minute = None

        self.re_postfix_status = re.compile(r'\bstatus=(\w*)\b')
        self.re_postfix_relay_localhost = re.compile(r'\brelay=[^\s\[]*\[(127\.0\.0\.1|::1)\]')
        self.re_postfix_client = re.compile('^[0-9A-Z]+: client=(\S+)')
        self.re_postfix_noqueue = re.compile('^(?:[0-9A-Z]+: |NOQUEUE: )?reject: ')
        self.re_postfix_reject_discard = re.compile('^[0-9A-Z]+: (?:reject|discard):')
        self.re_postfix_greylisted = re.compile('Gr[ea]ylisted')

        self.re_amavis_tag = re.compile(r'\btags=')
        self.re_amavis_spam1 = re.compile(r'^\([\w-]+\) (Passed|Blocked) SPAM(?:MY)?\b')
        self.re_amavis_spam2 = re.compile(r'^\([\w-]+\) (Passed|Not-Delivered)\b.*\bquarantine spam')
        self.re_amavis_virus1 = re.compile(r'^\([\w-]+\) (Passed |Blocked )?INFECTED\b')
        self.re_amavis_virus2 = re.compile(r'^\([\w-]+\) (Passed |Blocked )?BANNED\b')
        self.re_amavis_virus3 = re.compile(r'Virus found\b')

        self.re_rspamd_reject = re.compile(r'rspamd_task_write_log.*, \(default: ([TF]) \((.*?)\)')

        self.ignore_localhost = False

        # config
        self.rrdstep = 60
        self.xpoints = 540
        self.points_per_sample = 3

    def run(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--systemd-unit', dest='units', action='append',
                            help='systemd UNITs to filter for', metavar='UNIT', required=True)
        parser.add_argument('--daemon-rrd', dest='folder',
                            help='write RRDs to FOLDER', metavar='FOLDER', default='/var/lib/mailgraph')
        parser.add_argument('--ignore-localhost', dest='ignore_localhost', action='store_true',
                            help='ignore mail to/from localhost', default=False)
        parser.add_argument('-v', dest='level', action='store_const', const='debug',
                            help='Verbose output (same as --log-level=debug)')
        parser.add_argument('--log-level', metavar='LEVEL', default='info', dest='level',
                            choices=['', 'debug', 'info', 'warning', 'error', 'critical'],
                            help='Logging LEVEL to use')
        parser.add_argument('-c', '--cat', default=False, action='store_true',
                            help='Only import old data, do not watch for new data')

        args = parser.parse_args()
        logging.basicConfig(level=getattr(logging, args.level.upper()),
                            format='%(levelname)s: %(message)s')

        self.ignore_localhost = args.ignore_localhost

        os.chdir(args.folder)

        logging.debug('Attaching to journal')
        reader = journal.Reader()
        for unit in args.units:
            reader.add_match(_SYSTEMD_UNIT=unit)

        if not args.cat:
            logging.debug('Waiting for new data...')
            reader.seek_tail()
            reader.get_previous()
            while True:
                event = reader.wait(-1)
                if event == journal.APPEND:
                    self.process_entries(reader)
        else:
            logging.debug('Importing data...')
            self.process_entries(reader)

    def process_entries(self, reader):
        for entry in reader:
            self.process_entry(entry)

    def process_entry(self, entry):
        try:
            comm = entry['_COMM']
            text = entry['MESSAGE']
            unit = entry['_SYSTEMD_UNIT']
        except KeyError as e:
            logging.critical('{} not found in {}'.format(str(e), entry))
            return

        try:
            time = entry['_SOURCE_REALTIME_TIMESTAMP']
        except KeyError:
            time = entry['__REALTIME_TIMESTAMP']

        if unit.startswith('postfix'):
            # Enter postfix
            prog = comm
            n = self.re_postfix_status.search(text)
            if prog == 'smtp':
                if n:
                    if n.group(1) == 'sent':
                        if self.re_postfix_relay_localhost.search(text) and self.ignore_localhost:
                            return
                        self.event(time, 'sent')
                    if n.group(1) == 'bounced':
                        self.event(time, 'bounced')
            if prog == 'local':
                if n and n.group(1) == 'bounced':
                    self.event(time, 'bounced')
            if prog == 'smtpd':
                m = self.re_postfix_client.match(text)
                if m:
                    if re.search(r'\[127\.0\.0\.1\]', text) and self.ignore_localhost:
                        return
                    self.event(time, 'received')
                elif self.re_postfix_greylisted.search(text):
                    self.event(time, 'greylisted')
                elif self.re_postfix_noqueue.match(text):
                    self.event(time, 'rejected')
            if prog == 'error':
                if n and n.group(1) == 'bounced':
                    self.event(time, 'bounced')
            if prog == 'cleanup':
                if self.re_postfix_reject_discard.match(text):
                    self.event(time, 'rejected')
        elif unit.startswith('amavis'):
            if self.re_amavis_spam1.match(text):
                if not self.re_amavis_tag.search(text):
                    self.event(time, 'spam')
            elif self.re_amavis_spam2.match(text):
                self.event(time, 'spam')
            elif self.re_amavis_virus1.match(text):
                if not self.re_amavis_tag.search(text):
                    self.event(time, 'virus')
            elif self.re_amavis_virus2.match(text):
                if not self.re_amavis_tag.search(text):
                    self.event(time, 'virus')
            elif self.re_amavis_virus3.match(text):
                self.event(time, 'virus')
        elif unit.startswith('rspamd'):
            n = self.re_rspamd_reject.search(text);
            if n:
                if n.group(1) == 'T':
                    self.event(time, 'spam')
                elif n.group(1) == 'F':
                    if n.group(2) == 'greylist':
                        self.event(time, 'greylisted')
                    elif n.group(2) == 'add header':
                        self.event(time, 'spam')



    def event(self, time, event_name):
        if self.update(time):
            self.sum[event_name] += 1

    def update(self, time):
        t = int(time.timestamp())
        m = t - t % self.rrdstep

        if not self.rrd_inited:
            self.init_rrd(m)

        if m == self.this_minute:
            return 1

        if m < self.this_minute:
            return 0

        rrd_txt = '{sent:d}:{received:d}:{bounced:d}:{rejected:d}'.format(**self.sum)
        rrd_virus_txt = '{virus:d}:{spam:d}'.format(**self.sum)
        rrd_greylist_txt = '{greylisted:d}:{delayed:d}'.format(**self.sum)
        logging.debug('update {:d}:{}:{}:{}'.format(self.this_minute, rrd_txt, rrd_virus_txt, rrd_greylist_txt))
        rrdtool.update(self.rrd, '{:d}:{}'.format(self.this_minute, rrd_txt))
        rrdtool.update(self.rrd_virus, '{:d}:{}'.format(self.this_minute, rrd_virus_txt))
        rrdtool.update(self.rrd_greylist, '{:d}:{}'.format(self.this_minute, rrd_greylist_txt))
        if m > self.this_minute + self.rrdstep:
            for sm in range(self.this_minute + self.rrdstep, m, self.rrdstep):
                logging.debug('update {:d}:0:0:0:0:0:0 (SKIP)'.format(sm))
                rrdtool.update(self.rrd, '{:d}:0:0:0:0'.format(sm))
                rrdtool.update(self.rrd_virus, '{:d}:0:0'.format(sm))
                rrdtool.update(self.rrd_greylist, '{:d}:0:0'.format(sm))

        self.this_minute = m
        for k in self.sum:
            self.sum[k] = 0

        return 1

    def init_rrd(self, m):
        rows = self.xpoints / self.points_per_sample
        realrows = int(rows * 1.1)
        day_steps = int(3600 * 24 / (self.rrdstep * rows))
        week_steps = day_steps * 7
        month_steps = week_steps * 5
        year_steps = month_steps * 12

        if not os.path.isfile(self.rrd):
            rrdtool.create(self.rrd,
                           '--start', str(m - 1), '--step', str(self.rrdstep),
                           'DS:sent:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'DS:recv:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'DS:bounced:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'DS:rejected:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'RRA:AVERAGE:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(year_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(year_steps, realrows))
            self.this_minute = m
        else:
            self.this_minute = rrdtool.last(self.rrd) + self.rrdstep

        if not os.path.isfile(self.rrd_virus):
            rrdtool.create(self.rrd_virus,
                           '--start', str(m - 1), '--step', str(self.rrdstep),
                           'DS:virus:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'DS:spam:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'RRA:AVERAGE:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(year_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(year_steps, realrows))
        else:
            if not self.this_minute:
                self.this_minute = rrdtool.last(self.rrd_virus) + self.rrdstep

        if not os.path.isfile(self.rrd_greylist):
            rrdtool.create(self.rrd_greylist,
                           '--start', str(m - 1), '--step', str(self.rrdstep),
                           'DS:greylisted:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'DS:delayed:ABSOLUTE:{}:0:U'.format(2 * self.rrdstep),
                           'RRA:AVERAGE:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:AVERAGE:0.5:{}:{}'.format(year_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(day_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(week_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(month_steps, realrows),
                           'RRA:MAX:0.5:{}:{}'.format(year_steps, realrows))
        else:
            if not self.this_minute:
                self.this_minute = rrdtool.last(self.rrd_greylist) + self.rrdstep

        self.rrd_inited = True


if __name__ == '__main__':
    graph = Mailgraph()
    graph.run()
